# emobpy examples

**Exemplary application**

200 BEV profiles for Germany, as documented in DOI: 10.1038/s41597-021-00932-9

Link: [Zenodo](https://doi.org/10.5281/zenodo.3931663)

**Tutorial**

To see the basics of emobpy, please see the presentation in openmod folder.

**Instructions**

Git and Anaconda should be already installed.

Examples can be found in the folders. It is recommended to install everything in a conda environment with the following script in the command line or terminal.

    conda create --name bev python=3.6

activate the environment when using the tool

    conda activate bev

with the command line go to the place where you want to clone the emobpy_examples repository and type the following:

    git clone https://gitlab.com/diw-evu/emobpy/emobpy_examples.git

go into the downloaded repository

    cd emobpy_examples

Install emobpy

    pip install emobpy

Install the required packages from file requirements.txt

    pip install -r requirements.txt

 Run jupyter notebook and navigate through the folders

    jupyter notebook
